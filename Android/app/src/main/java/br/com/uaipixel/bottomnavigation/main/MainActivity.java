package br.com.uaipixel.bottomnavigation.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import br.com.uaipixel.bottomnavigation.R;
import br.com.uaipixel.bottomnavigation.tools.BottomNavigationBehavior;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView mNavigationBotton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationBotton = findViewById(R.id.navigationBotton);
        mNavigationBotton.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mNavigationBotton.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {

            switch (item.getItemId())
            {
                case R.id.navigation_principal:
//                    mFragmentAtual = mHomeFragment;
//                    carregaFragment(mFragmentAtual);
                    return true;

                case R.id.navigation_projetos:
//                    mFragmentAtual = mPropostaFragment;
//                    carregaFragment(mFragmentAtual);
                    return true;

                case R.id.navigation_notificar_problemas:
//                    mFragmentAtual = mNotificarProblemasFragment;
//                    carregaFragment(mFragmentAtual);
                    return true;

                case R.id.navigation_eventos:
//                    mFragmentAtual = mEventosFragment;
//                    carregaFragment(mFragmentAtual);
                    return true;

                case R.id.navigation_menu_completo:
//                    mFragmentAtual = mMenuCompletoFragment;
//                    carregaFragment(mFragmentAtual);
                    return true;
            }

            return false;
        }
    };

    private void carregaFragment(Fragment fragment) {
        // carregar o mFragmentAtual
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
        manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
